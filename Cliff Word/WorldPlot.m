function WorldPlot(Q)
%
% This function plots the the cliff world environment and the greedy path
% according to the Q-table value
%
% Q: Q-table which is 4*12*4 table. the first and second dimension relates
% to the agent's position (state) and the third dimension corresponds to
% the actions
%

figure(1);
clf
hold on

path = zeros(5,13);
Cstate.x = 4;  Cstate.y = 1;
Episodend = 0;
while Episodend == 0
       
    %% epsilon-Greedy Policy
    [~,Action] = max(Q(Cstate.x,Cstate.y,:));
    
    %% Interaction
    [Nstate,~,Episodend] = CliffWord(Cstate,Action);
    
    path(Cstate.x,Cstate.y) = path(Cstate.x,Cstate.y) +1;
    Cstate  = Nstate;
end
path(Cstate.x,Cstate.y) = path(Cstate.x,Cstate.y) +1;

%%
world = ones(5,13);
world(4,2:11) = -2;
pcolor(world-2*path)
colormap(gray(3))
axis ij
axis image
axis off

title('Cliff Walking')
hold off
